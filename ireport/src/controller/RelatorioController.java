package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JTextField;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import persistence.GenericDAO;

public class RelatorioController implements ActionListener{

	
	private JTextField tfEmpresa;
	
	public RelatorioController( JTextField tfEmpresa){
		
		this.tfEmpresa = tfEmpresa;
	}

	private void geraRelatorio(){
		String empresa = tfEmpresa.getText();
		String jasper = "C:\\TEMP\\reportCarros.jasper";
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("empresa", empresa);
		byte[] bytes = null;
		String saida = "C:\\TEMP\\relatorio.pdf";
		FileOutputStream arquivoSaida = null;

		try{
			arquivoSaida = new FileOutputStream(saida);
			JasperReport relatorio = (JasperReport) JRLoader.loadObjectFromFile(jasper);
			bytes = JasperRunManager.runReportToPdf(relatorio, parametros, new GenericDAO().getConnection());
			
			if( bytes != null){
				File f = new File(saida);
				if( f.exists() ){
					f.delete();
				}
				arquivoSaida.write(bytes);
			}
		} catch (IOException | JRException e){
			e.printStackTrace();
		} finally {
			try {
				arquivoSaida.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		geraRelatorio();

	}

}