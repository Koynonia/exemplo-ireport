package view;

import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.RelatorioController;

public class Principal extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfEmpresa;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tfEmpresa = new JTextField();
		tfEmpresa.setBounds(42, 92, 132, 20);
		contentPane.add(tfEmpresa);
		tfEmpresa.setColumns(10);
		
		JButton btnGerar = new JButton("Gerar");
		btnGerar.setBounds(212, 91, 89, 23);
		contentPane.add(btnGerar);
	
	ActionListener rc = new RelatorioController(tfEmpresa);
	
	btnGerar.addActionListener(rc);
	}
}
